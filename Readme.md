# Keyword Tracker

## Overview

This solution demonstrates how to scrape Google search results without a third party library.

The solution contains 3 projects:

- KeywordTracker.Api - .NET 6.0/ASP .NET Core Web API project exposing API for Keyword Searching and TypeScript client generation
- KeywordTracker.Core - Class library with models, services, extensions that are used by the Web API
- keywordtracker-client - Ionic/Vue3 front end that provides the user with the ability to do keyword searches and see search history

## System Requirements

You must have the following tools installed to run the solution

- SQL Server LocalDb
- .NET 6.0 SDK
- .nodejs, npm, Vue CLI, Ionic

Note: To install Ionic you can use ```npm install -g @ionic/cli``` once npm is installed.

## How to run

You can run the solution by opening by running Run.ps1 in the root folder of the repository and then browsing to the following URL in your web browser: 

Run.ps1 will perform the following tasks:

- Release build of KeywordTracker.Api project
- Run the KeywordTracker.Api application
- Runs npm install
- Starts keyword-tracker Vue app

## Notes

- Database Connection String can be found in ./KeywordTracker.Api/appsettings.json
- GoogleKeywordTrackerService uses regex to parse the HTML response from Google. This isn't the best method for parsing HTML, however this is due to the requirement of not using third party libraries such as HtmlAgilityPack.
- Database will automatically be created when the API is run via Migrate method. If you want to do this manually, an SQL script is provided in CreateDatabase.sql in the repository root 
- API is running at https://localhost:7180
- API documentation can be viewed at https://localhost:7180/swagger
- The Vue app can be accessed at http://localhost:8101
- Ionic was used for Vue project to save time as it has a good UI library out of the box
- To add further migrations use ```Add-Migration MySuperFeature -Context KeywordTracker.Core.KeywordTrackerDbContext -StartupProject KeywordTracker.Api -Project KeywordTracker.Api```