//----------------------
// <auto-generated>
//     Generated using the NSwag toolchain v13.15.10.0 (NJsonSchema v10.6.10.0 (Newtonsoft.Json v9.0.0.0)) (http://NSwag.org)
// </auto-generated>
//----------------------

/* tslint:disable */
/* eslint-disable */
// ReSharper disable InconsistentNaming

import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse, CancelToken } from 'axios';

export class Client {
    private instance: AxiosInstance;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(baseUrl?: string, instance?: AxiosInstance) {

        this.instance = instance ? instance : axios.create();

        this.baseUrl = baseUrl !== undefined && baseUrl !== null ? baseUrl : "https://localhost:7180";

    }

    /**
     * Search Keyword
     * @param searchPhrase (optional) 
     * @param url (optional) 
     * @return The keyword search result. A comma delimited list of index positions where the url parameter is found
     */
    searchKeyword(searchPhrase: string | null | undefined, url: string | null | undefined , cancelToken?: CancelToken | undefined): Promise<string> {
        let url_ = this.baseUrl + "/KeywordTracker?";
        if (searchPhrase !== undefined && searchPhrase !== null)
            url_ += "searchPhrase=" + encodeURIComponent("" + searchPhrase) + "&";
        if (url !== undefined && url !== null)
            url_ += "url=" + encodeURIComponent("" + url) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_: AxiosRequestConfig = {
            method: "POST",
            url: url_,
            headers: {
                "Accept": "application/json"
            },
            cancelToken
        };

        return this.instance.request(options_).catch((_error: any) => {
            if (isAxiosError(_error) && _error.response) {
                return _error.response;
            } else {
                throw _error;
            }
        }).then((_response: AxiosResponse) => {
            return this.processSearchKeyword(_response);
        });
    }

    protected processSearchKeyword(response: AxiosResponse): Promise<string> {
        const status = response.status;
        let _headers: any = {};
        if (response.headers && typeof response.headers === "object") {
            for (let k in response.headers) {
                if (response.headers.hasOwnProperty(k)) {
                    _headers[k] = response.headers[k];
                }
            }
        }
        if (status === 200) {
            const _responseText = response.data;
            let result200: any = null;
            let resultData200  = _responseText;
                result200 = resultData200 !== undefined ? resultData200 : <any>null;
    
            return Promise.resolve<string>(result200);

        } else if (status !== 200 && status !== 204) {
            const _responseText = response.data;
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }
        return Promise.resolve<string>(null as any);
    }

    /**
     * Get Search Results
     * @return List of previous search results
     */
    getSearchResults(  cancelToken?: CancelToken | undefined): Promise<KeywordSearchResult[]> {
        let url_ = this.baseUrl + "/KeywordTracker";
        url_ = url_.replace(/[?&]$/, "");

        let options_: AxiosRequestConfig = {
            method: "GET",
            url: url_,
            headers: {
                "Accept": "application/json"
            },
            cancelToken
        };

        return this.instance.request(options_).catch((_error: any) => {
            if (isAxiosError(_error) && _error.response) {
                return _error.response;
            } else {
                throw _error;
            }
        }).then((_response: AxiosResponse) => {
            return this.processGetSearchResults(_response);
        });
    }

    protected processGetSearchResults(response: AxiosResponse): Promise<KeywordSearchResult[]> {
        const status = response.status;
        let _headers: any = {};
        if (response.headers && typeof response.headers === "object") {
            for (let k in response.headers) {
                if (response.headers.hasOwnProperty(k)) {
                    _headers[k] = response.headers[k];
                }
            }
        }
        if (status === 200) {
            const _responseText = response.data;
            let result200: any = null;
            let resultData200  = _responseText;
            if (Array.isArray(resultData200)) {
                result200 = [] as any;
                for (let item of resultData200)
                    result200!.push(KeywordSearchResult.fromJS(item));
            }
            else {
                result200 = <any>null;
            }
            return Promise.resolve<KeywordSearchResult[]>(result200);

        } else if (status !== 200 && status !== 204) {
            const _responseText = response.data;
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }
        return Promise.resolve<KeywordSearchResult[]>(null as any);
    }

    /**
     * Clear Search Results
     * @return The search results have been cleared
     */
    clearSearchResults(  cancelToken?: CancelToken | undefined): Promise<void> {
        let url_ = this.baseUrl + "/KeywordTracker";
        url_ = url_.replace(/[?&]$/, "");

        let options_: AxiosRequestConfig = {
            method: "DELETE",
            url: url_,
            headers: {
            },
            cancelToken
        };

        return this.instance.request(options_).catch((_error: any) => {
            if (isAxiosError(_error) && _error.response) {
                return _error.response;
            } else {
                throw _error;
            }
        }).then((_response: AxiosResponse) => {
            return this.processClearSearchResults(_response);
        });
    }

    protected processClearSearchResults(response: AxiosResponse): Promise<void> {
        const status = response.status;
        let _headers: any = {};
        if (response.headers && typeof response.headers === "object") {
            for (let k in response.headers) {
                if (response.headers.hasOwnProperty(k)) {
                    _headers[k] = response.headers[k];
                }
            }
        }
        if (status === 200) {
            const _responseText = response.data;
            return Promise.resolve<void>(null as any);

        } else if (status !== 200 && status !== 204) {
            const _responseText = response.data;
            return throwException("An unexpected server error occurred.", status, _responseText, _headers);
        }
        return Promise.resolve<void>(null as any);
    }
}

export class KeywordSearchResult implements IKeywordSearchResult {
    id!: number;
    keywords!: string;
    url!: string;
    created!: Date;
    result!: string;
    resultCount!: number;

    constructor(data?: IKeywordSearchResult) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(_data?: any) {
        if (_data) {
            this.id = _data["id"] !== undefined ? _data["id"] : <any>null;
            this.keywords = _data["keywords"] !== undefined ? _data["keywords"] : <any>null;
            this.url = _data["url"] !== undefined ? _data["url"] : <any>null;
            this.created = _data["created"] ? new Date(_data["created"].toString()) : <any>null;
            this.result = _data["result"] !== undefined ? _data["result"] : <any>null;
            this.resultCount = _data["resultCount"] !== undefined ? _data["resultCount"] : <any>null;
        }
    }

    static fromJS(data: any): KeywordSearchResult {
        data = typeof data === 'object' ? data : {};
        let result = new KeywordSearchResult();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id !== undefined ? this.id : <any>null;
        data["keywords"] = this.keywords !== undefined ? this.keywords : <any>null;
        data["url"] = this.url !== undefined ? this.url : <any>null;
        data["created"] = this.created ? this.created.toISOString() : <any>null;
        data["result"] = this.result !== undefined ? this.result : <any>null;
        data["resultCount"] = this.resultCount !== undefined ? this.resultCount : <any>null;
        return data;
    }
}

export interface IKeywordSearchResult {
    id: number;
    keywords: string;
    url: string;
    created: Date;
    result: string;
    resultCount: number;
}

export class ApiException extends Error {
    message: string;
    status: number;
    response: string;
    headers: { [key: string]: any; };
    result: any;

    constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isApiException = true;

    static isApiException(obj: any): obj is ApiException {
        return obj.isApiException === true;
    }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): any {
    if (result !== null && result !== undefined)
        throw result;
    else
        throw new ApiException(message, status, response, headers, null);
}

function isAxiosError(obj: any | undefined): obj is AxiosError {
    return obj && obj.isAxiosError === true;
}