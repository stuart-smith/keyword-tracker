﻿namespace KeywordTracker.Core
{
    public class KeywordTrackerExtensions
    {
        public static bool IsValidSearchResult(string input)
        {
            var splitByCommaSpace = input.Split(",", StringSplitOptions.RemoveEmptyEntries);
            return splitByCommaSpace.All(e => int.TryParse(e.Trim(), out _));
        }

        public static int Parse(string input)
        {
            var splitByCommaSpace = input.Split(",", StringSplitOptions.RemoveEmptyEntries);
            var allNumbers = splitByCommaSpace.All(e => int.TryParse(e.Trim(), out _));
            if (allNumbers)
            {
                return splitByCommaSpace.Count(e => int.TryParse(e, out _));
            }

            return 0;
        }

        public static IEnumerable<int> GetValues(string input)
        {
            try
            {
                var splitByCommaSpace = input.Split(",", StringSplitOptions.RemoveEmptyEntries);
                var allNumbers = splitByCommaSpace.All(e => int.TryParse(e.Trim(), out _));
                if (allNumbers)
                {
                    List<int> positions = new List<int>();
                    foreach (var item in splitByCommaSpace)
                    {
                        positions.Add(int.Parse(item));
                    }
                    return positions;
                }
            }
            catch (Exception)
            {
            }

            return new int[] { 0 };
        }
    }
}
