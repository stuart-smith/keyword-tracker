﻿using Microsoft.EntityFrameworkCore;

namespace KeywordTracker.Core
{
    public interface ISearchResultService
    {
        /// <summary>
        /// Returns all search results stored in the database, you can optionally filter for keywords and the URL
        /// </summary>
        /// <param name="keywords">The keywords to use for the search. Leave this blank to return keyword search results for all keywords</param>
        /// <param name="url">The URL to find in the search results. Leave this blank to return keyword searches for all URLs</param>
        /// <returns>The list of search results optionally filtered by keywords and URL</returns>
        Task<IEnumerable<KeywordSearchResult>> GetAllSearchResults(string keywords = null, string url = null);

        /// <summary>
        /// Adds the Search Result to the database
        /// </summary>
        /// <param name="keywordSearchResult">The Search Result to add</param>
        /// <returns>The newly added Search Result</returns>
        Task<KeywordSearchResult> AddSearchResult(KeywordSearchResult keywordSearchResult);

        /// <summary>
        /// Deletes all Search Results in the database
        /// </summary>
        /// <returns></returns>
        Task ClearSearchResults();
    }

    public class SearchResultService : ISearchResultService
    {
        public SearchResultService(KeywordTrackerDbContext databaseContext)
        {
            DatabaseContext = databaseContext;
        }

        public KeywordTrackerDbContext DatabaseContext { get; }


        /// <summary>
        /// Adds the Search Result to the database
        /// </summary>
        /// <param name="keywordSearchResult">The Search Result to add</param>
        /// <returns>The newly added Search Result</returns>
        public async Task<KeywordSearchResult> AddSearchResult(KeywordSearchResult keywordSearchResult)
        {
            DatabaseContext.Add(keywordSearchResult);
            await DatabaseContext.SaveChangesAsync();
            return keywordSearchResult;
        }

        /// <summary>
        /// Deletes all Search Results in the database
        /// </summary>
        /// <returns></returns>
        public async Task ClearSearchResults()
        {
            DatabaseContext.RemoveRange(DatabaseContext.KeywordSearchResults);
            await DatabaseContext.SaveChangesAsync();
        }

        /// <summary>
        /// Returns all search results stored in the database, you can optionally filter for keywords and the URL
        /// </summary>
        /// <param name="keywords">The keywords to use for the search. Leave this blank to return keyword search results for all keywords</param>
        /// <param name="url">The URL to find in the search results. Leave this blank to return keyword searches for all URLs</param>
        /// <returns>The list of search results optionally filtered by keywords and URL</returns>
        public async Task<IEnumerable<KeywordSearchResult>> GetAllSearchResults(string keywords = null, string url = null)
        {
            var query = DatabaseContext.KeywordSearchResults.AsNoTracking();
            
            if(!string.IsNullOrWhiteSpace(keywords))
            {
                query = query.Where(e => e.Keywords == keywords);
            }
            
            if (!string.IsNullOrWhiteSpace(url))
            {
                query = query.Where(e => e.Url == keywords);
            }
            
            return await query.OrderByDescending(e => e.Created)
                .ToListAsync();
        }
    }
}