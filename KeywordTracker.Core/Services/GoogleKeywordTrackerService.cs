﻿using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace KeywordTracker.Core
{
    /// <summary>
    /// Google Keyword Tracker Service. This service uses the Google Search Engine to search for keywords
    /// </summary>
    public class GoogleKeywordTrackerService : KeywordTrackerService, IKeywordTrackerService
    {
        /// <summary>
        /// The Base URL of the Search Engine used
        /// </summary>
        private const string BaseUrl = "https://www.google.co.uk";

        /// <summary>
        /// Search for Keywords with Google
        /// </summary>
        /// <param name="searchPhrase">The keywords to search for</param>
        /// <param name="url">The URL to search for in the keyword search result</param>
        /// <param name="limit">The limit of results to return (defaults to 100)</param>
        /// <returns></returns>
        public override async Task<string> SearchKeyword(string searchPhrase, string url, int limit = 100)
        {
            var httpClient = new HttpClient();

            httpClient.BaseAddress = new Uri(BaseUrl);

            //needed to add this or google returns a dialog with no search results
            httpClient.DefaultRequestHeaders.Add("user-agent", Constants.MOST_COMMON_USER_AGENT);

            //perform google search with limit (default 100) and url encoded search phrase
            var searchWebResponse = await httpClient.GetAsync($"/search?num={limit}&q={HttpUtility.UrlEncode(searchPhrase)}");

            //read the result as string
            var responseAsString = await searchWebResponse.Content.ReadAsStringAsync();

            if (!IsValidSearchResultsHtml(responseAsString) || !searchWebResponse.IsSuccessStatusCode)
            {
                return $"The result from {BaseUrl} is not a valid Search Results page.";
            }

            return ProcessHtmlResponse(responseAsString, url);
        }


        /// <summary>
        /// Validates the HTML to see if it contains a valid search results response
        /// </summary>
        /// <param name="html">The HTML to validate</param>
        /// <returns>True if the HTML is a valid search result, False if the response cannot be parsed as a valid search result</returns>
        private bool IsValidSearchResultsHtml(string html)
        {
            return html.Contains("itemtype=\"http://schema.org/SearchResultsPage\"");
        }

        /// <summary>
        /// Parses the HTML and compute the result string
        /// </summary>
        /// <param name="html">The HTML to process</param>
        /// <param name="search">The string to search for in the result</param>
        /// <returns></returns>
        private string ProcessHtmlResponse(string html, string search)
        {
            List<string> urls = new List<string>();
            List<int> foundPositions = new List<int>();

            //remove most of the stuff we are not interested in so the html is easier to parse
            var regexReplacements = new[] { "<script.*?</script>", "<style.*?</style>", "<head.*?</head>", "<noscript.*?</noscript>", "<hr.*?>", @"<img\s[^>]*>(?:\s*?</img>)?" };
            var strReplacements = new[] { "<!doctype html>", "<br>" };
            
            foreach (var item in regexReplacements)
            {
                html = Regex.Replace(html, item, string.Empty, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            }

            foreach (var item in strReplacements)
            {
                html = html.Replace(item, string.Empty);
            }

            //find the div with the role=main and until <div id="bottomads
            var patternEnding = "<div id=\"bottomads\">";
            var pattern = "<div[^>]*role=\"main\".*?>(.*?)" + patternEnding;
            var mainDivMatch = Regex.Match(html, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            var fixedMainDivMatch = mainDivMatch.Value.Replace(patternEnding, string.Empty);

            //find all the anchor tags
            var anchorTagsInMainDivMatches = Regex.Matches(fixedMainDivMatch, "<a.*?</a>", RegexOptions.Singleline | RegexOptions.IgnoreCase);

            //for each anchor tag in the role=main div, check to see if the href is not a google link etc... and if so add to list of urls
            foreach (Match anchorTagMatch in anchorTagsInMainDivMatches)
            {
                var anchorTag = XElement.Parse(anchorTagMatch.Value);
                string? href = anchorTag?.Attribute("href")?.Value;
                if (href?.StartsWith("http") == true && !href.StartsWith("/") == true && !string.IsNullOrWhiteSpace(href))
                {
                    urls.Add(href);
                }
            }

            //go through each url and check if it contains the search text and add its position + 1 to the list
            for (int i = 0; i < urls.Count; i++)
            {
                if (urls[i].Contains(search, StringComparison.OrdinalIgnoreCase))
                {
                    foundPositions.Add(i + 1);
                }
            }

            return foundPositions.Any() ? string.Join(", ", foundPositions) : "0";
        }
    }
}