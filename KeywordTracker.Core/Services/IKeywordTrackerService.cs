﻿namespace KeywordTracker.Core
{
    public interface IKeywordTrackerService
    {
        /// <summary>
        /// Search for Keywords
        /// </summary>
        /// <param name="searchPhrase">The keywords to search for</param>
        /// <param name="url">The URL to search for in the keyword search result</param>
        /// <param name="limit">The limit of results to return (defaults to 100)</param>
        /// <returns></returns>
        Task<string> SearchKeyword(string searchPhrase, string url, int limit = 100);
    }

    public abstract class KeywordTrackerService : IKeywordTrackerService
    {
        /// <summary>
        /// Search for Keywords
        /// </summary>
        /// <param name="searchPhrase">The keywords to search for</param>
        /// <param name="url">The URL to search for in the keyword search result</param>
        /// <param name="limit">The limit of results to return (defaults to 100)</param>
        /// <returns></returns>
        public abstract Task<string> SearchKeyword(string searchPhrase, string url, int limit = 100);
    }
}