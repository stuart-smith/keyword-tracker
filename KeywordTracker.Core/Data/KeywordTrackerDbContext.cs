﻿using Microsoft.EntityFrameworkCore;

namespace KeywordTracker.Core
{
    public class KeywordTrackerDbContext : DbContext
    {
        public DbSet<KeywordSearchResult> KeywordSearchResults { get; set; }

        public KeywordTrackerDbContext(DbContextOptions<KeywordTrackerDbContext> options) : base(options)
        {
        }
    }
}
