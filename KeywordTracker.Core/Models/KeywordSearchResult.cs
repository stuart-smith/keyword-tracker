﻿namespace KeywordTracker.Core
{
    /// <summary>
    /// The Keyword Search Result that is stored in the database
    /// </summary>
    public class KeywordSearchResult
    {
        /// <summary>
        /// The Unique (database) ID of the Search Result
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The keywords used in the search
        /// </summary>
        public string Keywords { get; set; }


        /// <summary>
        /// The URL to search for in the Search Result
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// The UTC Date/Time when the search was performed
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// The raw result from the KeywordTrackerService
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// The count of results found
        /// </summary>
        public int ResultCount { get; set; }
    }
}
