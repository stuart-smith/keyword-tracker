﻿namespace KeywordTracker.Core
{
    public static class Constants
    {
        public const string MOST_COMMON_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36";
        public const string CONNECTION_STRING_NAME = "SqlServerDatabase";
        public const string API_VERSION = "v1";
        public const string API_TITLE = "InfoTrack Keyword Tracker";
        public const string API_DESCRIPTION = "A simple API to demonstrate how to parse Google search results without a library";
        public const string API_CONTACT_NAME = "Stuart Smith";
        public const string API_CONTACT_EMAIL = "stuartsmithuk@hotmail.co.uk";
    }
}