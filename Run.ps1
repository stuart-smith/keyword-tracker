dotnet publish ./KeywordTracker.sln -c Release
Start-Process -FilePath "dotnet" -ArgumentList @("KeywordTracker.Api.dll", "urls=https://localhost:7180") -WorkingDirectory ".\KeywordTracker.Api\bin\Release\net6.0"
cd .\keywordtracker-client
npm install
ionic serve --port 8101
