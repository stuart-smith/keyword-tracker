﻿using KeywordTracker.Core;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace KeywordTracker.Api
{
    [ApiController]
    [Route("[controller]")]
    public class KeywordTrackerController : Controller
    {
        public KeywordTrackerController(IKeywordTrackerService keywordTrackerService,
            ISearchResultService searchResultService)
        {
            KeywordTrackerService = keywordTrackerService;
            SearchResultService = searchResultService;
        }

        public IKeywordTrackerService KeywordTrackerService { get; }
        public ISearchResultService SearchResultService { get; }

        [HttpPost]
        [OpenApiOperation(nameof(SearchKeyword), "Search Keyword", "Performs Search Engine query for search phrase containing the specified URL")]
        [SwaggerResponse(200, typeof(string), Description = "The keyword search result. A comma delimited list of index positions where the url parameter is found")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> SearchKeyword(string searchPhrase, string url)
        {
            var result = await KeywordTrackerService.SearchKeyword(searchPhrase, url);
            await SearchResultService.AddSearchResult(new KeywordSearchResult
            {
                Keywords = searchPhrase,
                Url = url,
                Created = DateTime.UtcNow,
                Result = result,
                ResultCount = KeywordTrackerExtensions.Parse(result),
            });
            return Ok(result);
        }
        [HttpGet]
        [OpenApiOperation(nameof(GetSearchResults), "Get Search Results", "Returns a list previous searches")]
        [SwaggerResponse(200, typeof(IEnumerable<KeywordSearchResult>), Description = "List of previous search results")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> GetSearchResults() => Ok(await SearchResultService.GetAllSearchResults());

        [HttpDelete]
        [OpenApiOperation(nameof(ClearSearchResults), "Clear Search Results", "Clears the list of Search Results")]
        [SwaggerResponse(200, typeof(void), Description = "The search results have been cleared")]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> ClearSearchResults()
        {
            await SearchResultService.ClearSearchResults();
            return Ok();
        }
    }
}
