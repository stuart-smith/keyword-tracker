﻿using KeywordTracker.Api.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace KeywordTracker.Api
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ScriptsController : ControllerBase
    {
        public IHttpContextAccessor HttpContextAccessor { get; }
        public IClientService ClientService { get; }

        public ScriptsController(IHttpContextAccessor httpContextAccessor,
            IClientService clientService)
        {
            HttpContextAccessor = httpContextAccessor;
            ClientService = clientService;
        }

        [HttpGet]
        [Route("api.ts")]
        public async Task<IActionResult> GenerateTypeScriptClient()
        {
            var baseUrl = $"{HttpContextAccessor.HttpContext.Request.Scheme}://{HttpContextAccessor.HttpContext.Request.Host}{HttpContextAccessor.HttpContext.Request.PathBase}";
            string url = $"{baseUrl}/swagger/v1/swagger.json";
            var code = await ClientService.GenerateTypeScriptClient(url);
            return File(Encoding.UTF8.GetBytes(code), "text/plain");
        }
    }
}
