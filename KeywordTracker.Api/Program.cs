using KeywordTracker.Api.Services;
using KeywordTracker.Core;
using Microsoft.EntityFrameworkCore;

//new .net 6.0 style program/startup - not a personal fan of this ;)

var builder = WebApplication.CreateBuilder(args);

//add controllers for mvc and dbcontext for SQL server (ef core)
builder.Services.AddControllers();
builder.Services.AddDbContext<KeywordTrackerDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString(Constants.CONNECTION_STRING_NAME), b => b.MigrationsAssembly("KeywordTracker.Api"));
});

//add cors, httpcontext accessor and configure swagger generation
builder.Services.AddCors();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSwaggerDocument(config =>
{
    config.PostProcess = document =>
    {
        document.Info.Version = Constants.API_VERSION;
        document.Info.Title = Constants.API_TITLE;
        document.Info.Description = Constants.API_DESCRIPTION;

        document.Info.Contact = new NSwag.OpenApiContact
        {
            Name = Constants.API_CONTACT_NAME,
            Email = Constants.API_CONTACT_EMAIL,
        };
    };
});

//add app services
builder.Services.AddTransient<IKeywordTrackerService, GoogleKeywordTrackerService>();
builder.Services.AddTransient<ISearchResultService, SearchResultService>();
builder.Services.AddTransient<IClientService, ClientService>();

var app = builder.Build();

//use swagger
app.UseOpenApi();
app.UseSwaggerUi3();

app.UseHttpsRedirection();

//use cors for client api
app.UseCors(x => x.AllowAnyMethod()
                .AllowAnyHeader()
                .AllowAnyOrigin());

//set up endpoints for controllers
app.MapControllers();

//run migrations
using (var serviceScope = app.Services.GetService<IServiceScopeFactory>().CreateScope())
{
    var context = serviceScope.ServiceProvider.GetRequiredService<KeywordTrackerDbContext>();
    context.Database.Migrate();
}
app.Run();
public partial class Program { }
