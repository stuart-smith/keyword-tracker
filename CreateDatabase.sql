﻿USE master;
GO
CREATE DATABASE KeywordTrackerDatabase;
GO
USE KeywordTrackerDatabase;
GO

IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [KeywordSearchResults] (
    [Id] bigint NOT NULL IDENTITY,
    [Keywords] nvarchar(max) NOT NULL,
    [Url] nvarchar(max) NOT NULL,
    [Created] datetime2 NOT NULL,
    [Result] nvarchar(max) NOT NULL,
    [ResultCount] int NOT NULL,
    CONSTRAINT [PK_KeywordSearchResults] PRIMARY KEY ([Id])
);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20220411084248_InitialCreate', N'6.0.3');
GO

COMMIT;
GO

